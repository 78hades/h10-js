let tabsTitle = document.querySelectorAll(`.tabs-title`);
let tabsContent = document.querySelectorAll(`.tabs-content`);
function switchUsers() {
  tabsTitle.forEach((user) => {
    user.addEventListener(`click`, function () {
      tabsTitle.forEach((user) => {
        user.classList.remove("active");
      });
      user.classList.add(`active`);
      let dataTab = user.getAttribute(`data-name`);
      tabsContent.forEach((content) => {
        content.style.display = `none`;
      });
      document.getElementById(`tabs-content-${dataTab}`).style.display = `block`;
    });
  });
}
switchUsers();
